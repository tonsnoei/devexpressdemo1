﻿using DevExpress.Xpo;

public class MyPerson : XPObject
{
    public MyPerson(Session session) : base(session)
    { }


    string lastName;
    string name;

    public string Name
    {
        get => name;
        set => SetPropertyValue(nameof(Name), ref name, value);
    }

    
    public string LastName
    {
        get => lastName;
        set => SetPropertyValue(nameof(LastName), ref lastName, value);
    }
}